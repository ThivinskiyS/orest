function placeMarker(coordX, coordY, map) {
    var image = new google.maps.MarkerImage('images/push_pin.png',
        new google.maps.Size(128, 128),
        new google.maps.Point(0, 0),
        new google.maps.Point(31, 62));
    var label = getLabelForMarker();
    var position = new google.maps.LatLng(coordX, coordY);
    var marker = new MarkerWithLabel({
        position: position,
        map: map,
        icon: image,
        labelContent: label,
        labelAnchor: new google.maps.Point(60, 40),
        labelClass: "labels",
        labelStyle: {opacity: 0.75}
    });
    map.panTo(position);
}