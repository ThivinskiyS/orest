function recalculateDistance(arrayOfMarkers) {
    var radius = $('#radius')[0].value;
    var table = $('#in-defined-radius');
    while (table.find('tr').length > 0) {
        table.find('tr').remove();
    }
    table = $('#out-defined-radius');
    while (table.find('tr').length > 0) {
        table.find('tr').remove();
    }
    for (var index = 0; index < arrayOfMarkers.length; index++) {
        var marker = arrayOfMarkers[index];
        addMarkerToList(marker.X, marker.Y, marker.title, radius);
    }
}
