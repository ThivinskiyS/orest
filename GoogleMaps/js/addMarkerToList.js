function addMarkerToList(coordX, coordY, title, radius) {
    var existingDistance, recordsList;
    var distance = google.maps.geometry.spherical.computeDistanceBetween(myLocation,  new google.maps.LatLng(coordX, coordY)) / 1000;
    var smallerThenRadius = $('#in-defined-radius');
    var largerThenRadius = $('#out-defined-radius');
    var newTD = document.createElement('td');
    var newRecord = document.createElement('tr');
    newTD.innerHTML = title + ': ' + coordX.toFixed(4) + ', ' + coordY.toFixed(4) + ' (' + distance.toFixed(1) + ' km)';
    newRecord.appendChild(newTD);

    if (distance >= radius) {
        recordsList = largerThenRadius.find('tr');
        if (recordsList.length == 0) {
            largerThenRadius.append(newRecord);
        } else {
            existingDistance = largerThenRadius.find('tr').last().text().match(/\(.+km\)/);
            existingDistance = existingDistance.toString().substr(1, existingDistance.toString().indexOf(' ') - 1);
            if (distance > existingDistance) {
                largerThenRadius.append(newRecord);
            }
            for (var index = 0; index < recordsList.length; index++) {
                existingDistance = recordsList.eq(index).text().match(/\(.+km\)/);
                existingDistance = existingDistance.toString().substr(1, existingDistance.toString().indexOf(' ') - 1);
                if (distance < existingDistance) {
                    largerThenRadius.find('tr').eq(index).before(newRecord);
                    break;
                }
            }
        }
    } else {
        recordsList = smallerThenRadius.find('tr');
        if (recordsList.length == 0) {
            smallerThenRadius.append(newRecord);
        } else {
            existingDistance = smallerThenRadius.find('tr').last().text().match(/\(.+km\)/);
            existingDistance = existingDistance.toString().substr(1, existingDistance.toString().indexOf(' ') - 1);
            if (distance > existingDistance) {
                smallerThenRadius.append(newRecord);
            }
            for (var index = 0; index < recordsList.length; index++) {
                existingDistance = recordsList.eq(index).text().match(/\(.+km\)/);
                existingDistance = existingDistance.toString().substr(1, existingDistance.toString().indexOf(' ') - 1);
                if (distance < existingDistance) {
                    smallerThenRadius.find('tr').eq(index).before(newRecord);
                    break;
                }
            }
        }
    }
}
