var myLocation;
function initLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(initialize);
    } else {
        myLocation = new google.maps.LatLng(50.4391, 30.5544);
    }
}