var arrayOfMarkers = [];
function initialize(position) {
    myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    $('#location').html('Your Location: ' + position.coords.latitude.toFixed(4) + ', ' + position.coords.longitude.toFixed(4));
    var mapCanvas = $("#map-canvas")[0];
    var mapOptions = {
        center: myLocation,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position: myLocation,
        map: map,
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
        draggable: true,
        title: 'You!'
    });

    google.maps.event.addListener(map, 'click', function (e) {
        placeMarker(e.latLng.A, e.latLng.F, map);
        var marker = {
            X: e.latLng.A,
            Y: e.latLng.F,
            title: getLabelForMarker()
        };
        arrayOfMarkers.push(marker);
        addMarkerToList(e.latLng.A, e.latLng.F, getLabelForMarker(), $('#radius')[0].value);
    });

    google.maps.event.addListener(marker, 'dragend', function () {
        myLocation = marker.getPosition();
        $('#location').html('Your Location: ' + myLocation.A.toFixed(4) + ', ' + myLocation.F.toFixed(4));
        recalculateDistance(arrayOfMarkers);
    });
    buildResultPage();
}