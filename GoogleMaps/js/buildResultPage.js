function buildResultPage(){
    var mainDiv = $('#left');
    var div = document.createElement('div');
    var input = document.createElement('input');
    var p = document.createElement('p');
    var text = document.createTextNode('Radius ');
    var table = document.createElement('table');

    input.id = 'radius';
    input.value = 25;
    input.type = 'text';
    div.id = 'radius-div';
    mainDiv.append(div);
    div.appendChild(text);
    div.appendChild(input);
    text = document.createTextNode(' km');
    div.appendChild(text);

    div = document.createElement('div');
    div.className = 'scroll';
    table.id = 'in-defined-radius';
    mainDiv.append(div);
    div.appendChild(table);

    p.innerHTML = 'Out of defined radius: ';
    mainDiv.append(p);
    div = document.createElement('div');
    div.className = 'scroll';
    table = document.createElement('table');
    table.id = 'out-defined-radius';
    mainDiv.append(div);
    div.appendChild(table);

    $('#radius').change(function () {
        recalculateDistance(arrayOfMarkers)
    });
}